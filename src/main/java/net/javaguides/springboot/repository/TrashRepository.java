package net.javaguides.springboot.repository;

import net.javaguides.springboot.model.TrashEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrashRepository extends JpaRepository<TrashEntity, String> {
}
