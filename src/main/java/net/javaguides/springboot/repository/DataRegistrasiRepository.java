package net.javaguides.springboot.repository;


import net.javaguides.springboot.model.DataRegistrasiEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface DataRegistrasiRepository extends JpaRepository<DataRegistrasiEntity, Long> {

    Page<DataRegistrasiEntity> findByJurusan (String jurusan, Pageable pageable);
    List<DataRegistrasiEntity> findByJurusan (String jurusan);

    @Query(value = "SELECT dr from DataRegistrasiEntity dr where dr.jurusan = :jurusan")
    Page<DataRegistrasiEntity> findByOrderByCreatedAtAsc(String jurusan, Pageable pageable);

}
