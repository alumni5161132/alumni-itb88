package net.javaguides.springboot.enums;

public enum JurusanEnum {
    ASTRONOMI ("Astronomi"),
    MATEMATIKA ("Matematika"),
    FISIKA ("Fisika"),
    KIMIA ("Kimia"),
    BIOLOGI ("Biologi"),
    FARMASI ("Farmasi"),
    GEOFISIKA_METEOROLOGI("Geofisika dan Meteorologi"),
    TEKNIK_GEOLOGI ("Teknik Geologi"),
    TEKNIK_PERTAMBANGAN ("Teknik Pertambangan"),
    TEKNIK_PERMINYAKAN ("Teknik Perminyakan"),
    TEKNIK_MESIN ("Teknik Mesin"),
    TEKNIK_KIMIA ("Teknik Kimia"),
    TEKNIK_ELEKTRO ("Teknik Elektro"),
    TEKNIK_FISIKA ("Teknik Fisika"),
    TEKNIK_INFORMATIKA ("Teknik Informatika"),
    TEKNIK_INDUSTRI ("Teknik Industri"),
    TEKNIK_SIPIL ("Teknik Sipil"),
    TEKNIK_GEODESI ("Teknik Geodesi"),
    TEKNIK_ARSITEKTUR ("Teknik Arsitektur"),
    TEKNIK_PLANOLOGI ("Teknik Planologi"),
    TEKNIK_LINGKUNGAN ("Teknik Lingkungan"),
    FSRD ("FSRD");

    private final String value;

    JurusanEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
