package net.javaguides.springboot.enums;

public enum PrintBy {
    ALL_PRINT ("Tanggal"),
    JURUSAN_PRINT ("Jurusan");

    private final String value;

    PrintBy (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
