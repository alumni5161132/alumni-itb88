package net.javaguides.springboot.enums;

public enum Ukuran {
    XS ("XS"),
    S ("S"),
    M ("M"),
    L ("L"),
    XL ("XL"),
    XXL ("XXL"),
    XL4 ("4XL"),
    XL5 ("5XL");

    private final String value;

    Ukuran (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
