package net.javaguides.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="trash_data")
public class TrashEntity {
    @Id
    private String id;

    private Long idData;

    @Column(nullable=false)
    private String nama;

    @Column(nullable=false)
    private String noHp;

    @Column(nullable=false)
    private String alamat;

    @Column(nullable=false)
    private String jurusan;

    @Column(nullable=false)
    private String ukuranKemeja;

    @Column(nullable=false)
    private Long createdAt;
}
