package net.javaguides.springboot.web.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRegistrasiDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    @NotBlank(message = "Nama harus diisi")
    @Size(min = 1, max = 100, message = "Nama tidak boleh lebih dari 100 karakter")
    private String nama;

    @Column(nullable=false)
    @Pattern(regexp = "\\d{10}[0-9]", message = "Nomer hp minimal 10 angka")
    private String phoneNumber;
    private String noHp;

    @Column(nullable=false)
    @NotBlank(message = "Alamat harus diisi")
    @Size(min = 3, max = 100, message = "Alamat harus antara 3 sampai 100 karakter")
    private String alamat;

    @Column(nullable=false)
    private String jurusan;

    @Column(nullable=false)
    private String ukuranKemeja;
}
