package net.javaguides.springboot.web;

import net.javaguides.springboot.enums.JurusanEnum;
import net.javaguides.springboot.model.DataRegistrasiEntity;
import net.javaguides.springboot.repository.DataRegistrasiRepository;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class PrintDataController {
    @Autowired
    DataRegistrasiRepository repository;

    private TemplateEngine templateEngine;

    @Autowired
    public void PdfController(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @GetMapping("/print-data")
    public void print(Model model, HttpServletResponse response, @RequestParam(name = "printBy", required = false) String printBy) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userRole = auth.getAuthorities().iterator().next().getAuthority();

        List<DataRegistrasiEntity> pagedData = new ArrayList<>();
        if(userRole.equalsIgnoreCase("ROLE_ADMIN")){
            if(printBy.equalsIgnoreCase("Jurusan")){
                pagedData = repository.findAll(Sort.by(Sort.Direction.ASC, "jurusan"));
            }else{
                pagedData = repository.findAll(Sort.by(Sort.Direction.ASC, "createdAt"));
            }
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINAS")) {
            pagedData = repository.findByJurusan(JurusanEnum.ASTRONOMI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINMA")) {
            pagedData = repository.findByJurusan(JurusanEnum.MATEMATIKA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINFI")) {
            pagedData = repository.findByJurusan(JurusanEnum.FISIKA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINKI")) {
            pagedData = repository.findByJurusan(JurusanEnum.KIMIA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINBI")) {
            pagedData = repository.findByJurusan(JurusanEnum.BIOLOGI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINFA")) {
            pagedData = repository.findByJurusan(JurusanEnum.FARMASI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINGM")) {
            pagedData = repository.findByJurusan(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINGL")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_GEOLOGI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTA")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTM")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_PERMINYAKAN.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINMS")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_MESIN.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTK")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_KIMIA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINEL")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_ELEKTRO.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTF")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_FISIKA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINIF")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_INFORMATIKA.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTI")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_INDUSTRI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINSI")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_SIPIL.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINGD")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_GEODESI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINAR")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_ARSITEKTUR.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINPL")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_PLANOLOGI.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINTL")) {
            pagedData = repository.findByJurusan(JurusanEnum.TEKNIK_LINGKUNGAN.getValue());
        }else if (userRole.equalsIgnoreCase("ROLE_ADMINFSRD")) {
            pagedData = repository.findByJurusan(JurusanEnum.FSRD.getValue());
        }else{
            pagedData = repository.findAll();
        }
        model.addAttribute("data", pagedData);

        Date tgl = new Date();

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Item Data");

        // Create header row
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("No");
        headerRow.createCell(1).setCellValue("Tanggal");
        headerRow.createCell(2).setCellValue("Nama");
        headerRow.createCell(3).setCellValue("No Hp");
        headerRow.createCell(4).setCellValue("Alamat");
        headerRow.createCell(5).setCellValue("Jurusan");
        headerRow.createCell(6).setCellValue("Ukuran Kemeja");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // Populate data rows
        int rowNum = 1;
        for (DataRegistrasiEntity data : pagedData) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(rowNum - 1);
            long timestamp = data.getCreatedAt();
            Date date = new Date(timestamp);
            String formattedDate = dateFormat.format(date);
            row.createCell(1).setCellValue(formattedDate);
            row.createCell(2).setCellValue(data.getNama());
            row.createCell(3).setCellValue(data.getNoHp());
            row.createCell(4).setCellValue(data.getAlamat());
            row.createCell(5).setCellValue(data.getJurusan());
            row.createCell(6).setCellValue(data.getUkuranKemeja());
        }

        // Set response headers
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=data.xlsx");

        // Write workbook to response output stream
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/print-data-by-jurusan")
    public void printByJurusan(Model model, HttpServletResponse response) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userRole = auth.getAuthorities().iterator().next().getAuthority();

        List<DataRegistrasiEntity> pagedData = new ArrayList<>();
        pagedData = repository.findAll(Sort.by(Sort.Direction.ASC, "jurusan"));
        model.addAttribute("data", pagedData);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Item Data");

        // Create header row
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("No");
        headerRow.createCell(1).setCellValue("Nama");
        headerRow.createCell(2).setCellValue("No Hp");
        headerRow.createCell(3).setCellValue("Alamat");
        headerRow.createCell(4).setCellValue("Jurusan");
        headerRow.createCell(5).setCellValue("Ukuran Kemeja");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // Populate data rows
        int rowNum = 1;
        for (DataRegistrasiEntity data : pagedData) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(rowNum - 1);
            row.createCell(1).setCellValue(data.getNama());
            row.createCell(2).setCellValue(data.getNoHp());
            row.createCell(3).setCellValue(data.getAlamat());
            row.createCell(4).setCellValue(data.getJurusan());
            row.createCell(5).setCellValue(data.getUkuranKemeja());
        }

        // Set response headers
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=data.xlsx");

        // Write workbook to response output stream
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/print-total-data")
    public ModelAndView generatePdf() throws Exception {
        List<Data> datas = new ArrayList<>();
        datas.add(new Data("Astronomi", repository.findByJurusan(JurusanEnum.ASTRONOMI.getValue()).size()));
        datas.add(new Data("Matematika", repository.findByJurusan(JurusanEnum.MATEMATIKA.getValue()).size()));
        datas.add(new Data("Fisika", repository.findByJurusan(JurusanEnum.FISIKA.getValue()).size()));
        datas.add(new Data("Kimia", repository.findByJurusan(JurusanEnum.KIMIA.getValue()).size()));
        datas.add(new Data("Biologi", repository.findByJurusan(JurusanEnum.BIOLOGI.getValue()).size()));
        datas.add(new Data("Farmasi", repository.findByJurusan(JurusanEnum.FARMASI.getValue()).size()));
        datas.add(new Data("Geofisika dan Meteorologi", repository.findByJurusan(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue()).size()));
        datas.add(new Data("Teknik Geologi", repository.findByJurusan(JurusanEnum.TEKNIK_GEOLOGI.getValue()).size()));
        datas.add(new Data("Teknik Pertambangan", repository.findByJurusan(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue()).size()));
        datas.add(new Data("Teknik Perminyakan", repository.findByJurusan(JurusanEnum.TEKNIK_PERMINYAKAN.getValue()).size()));
        datas.add(new Data("Teknik Mesin", repository.findByJurusan(JurusanEnum.TEKNIK_MESIN.getValue()).size()));
        datas.add(new Data("Teknik Kimia", repository.findByJurusan(JurusanEnum.TEKNIK_KIMIA.getValue()).size()));
        datas.add(new Data("Teknik Elektro", repository.findByJurusan(JurusanEnum.TEKNIK_ELEKTRO.getValue()).size()));
        datas.add(new Data("Teknik Fisika", repository.findByJurusan(JurusanEnum.TEKNIK_FISIKA.getValue()).size()));
        datas.add(new Data("Teknik Informatika", repository.findByJurusan(JurusanEnum.TEKNIK_INFORMATIKA.getValue()).size()));
        datas.add(new Data("Teknik Industri", repository.findByJurusan(JurusanEnum.TEKNIK_INDUSTRI.getValue()).size()));
        datas.add(new Data("Teknik Sipil", repository.findByJurusan(JurusanEnum.TEKNIK_SIPIL.getValue()).size()));
        datas.add(new Data("Teknik Geodesi", repository.findByJurusan(JurusanEnum.TEKNIK_GEODESI.getValue()).size()));
        datas.add(new Data("Teknik Arsitektur", repository.findByJurusan(JurusanEnum.TEKNIK_ARSITEKTUR.getValue()).size()));
        datas.add(new Data("Teknik Planologi", repository.findByJurusan(JurusanEnum.TEKNIK_PLANOLOGI.getValue()).size()));
        datas.add(new Data("Teknik Lingkungan", repository.findByJurusan(JurusanEnum.TEKNIK_LINGKUNGAN.getValue()).size()));
        datas.add(new Data("FSRD", repository.findByJurusan(JurusanEnum.FSRD.getValue()).size()));


        Context context = new Context();
        context.setVariable("datas", datas);
        context.setVariable("data", repository.count());

        String templateHtml = templateEngine.process("pdf-template", context);

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(templateHtml);
        renderer.layout();

        ModelAndView modelAndView = new ModelAndView(new PdfView(renderer));

        return modelAndView;
    }

    // Model class for your data
    public class Data {
        private String jurusan;
        private Integer total;

        public Data(String jurusan, Integer total) {
            this.jurusan = jurusan;
            this.total = total;
        }

        // getter methods

        public String getJurusan() {
            return jurusan;
        }

        public Integer getTotal() {
            return total;
        }
    }

}
