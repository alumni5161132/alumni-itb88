package net.javaguides.springboot.web;

import org.springframework.web.servlet.view.AbstractView;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Map;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class PdfView extends AbstractView {

    private final ITextRenderer renderer;

    public PdfView(ITextRenderer renderer) {
        this.renderer = renderer;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        renderer.createPDF(os);

        byte[] content = os.toByteArray();

        response.setContentType("application/pdf");
        response.setContentLength(content.length);

        OutputStream out = response.getOutputStream();
        FileCopyUtils.copy(content, out);

        os.close();
    }
}

