package net.javaguides.springboot.web;

import net.javaguides.springboot.enums.JurusanEnum;
import net.javaguides.springboot.model.DataRegistrasiEntity;
import net.javaguides.springboot.model.TrashEntity;
import net.javaguides.springboot.repository.DataRegistrasiRepository;
import net.javaguides.springboot.repository.TrashRepository;
import net.javaguides.springboot.service.DataService;
import net.javaguides.springboot.web.dto.DataRegistrasiDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Controller
public class DataController {
    @Autowired
    DataService dataService;

    @Autowired
    DataRegistrasiRepository repository;

    @Autowired
    TrashRepository trashRepository;


    @PostMapping("/save")
    public String postData(@Valid @ModelAttribute("data") DataRegistrasiDTO data,
                           BindingResult result,
                           Model model){
        if (result.hasErrors()) {
            model.addAttribute("data", data);
            return "redirect:/form-registrasi?gagal";
        }
        dataService.saveData(data);
        return "redirect:/form-registrasi?success";
    }

    @GetMapping("/edit/{id}")
    public String editData(@PathVariable Long id, Model model) {
        DataRegistrasiEntity data = dataService.findById(id);
        model.addAttribute("data", data);
        return "edit-form";
    }

    @PostMapping("/update/{id}")
    public String updateData(@PathVariable Long id, @ModelAttribute DataRegistrasiDTO updatedData) {
        dataService.updateData(id, updatedData);
        return "redirect:/super-admin";
    }

    @GetMapping("/delete/{id}")
    public String deleteData(@PathVariable Long id) {
        dataService.deleteData(id);
        return "redirect:/super-admin";
    }

    @GetMapping("/sync/{id}")
    public String syncData(@PathVariable String id) {
        dataService.savefromtrash(id);
        return "redirect:/super-admin";
    }

    @GetMapping("/super-admin")
    public String listData(Model model,  @RequestParam(defaultValue = "0") int page,
                           @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                           @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir, @RequestParam(name = "jurusan", required = false) String jurusan){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> pagedData = repository.findAll(pageable);
        Page<TrashEntity> dataTrash = trashRepository.findAll(pageable);
        long currentPage = pagedData.getNumber()+1;
        model.addAttribute("data", pagedData);
        model.addAttribute("dataTrash", dataTrash);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        model.addAttribute("totDat", repository.count());

        //Data per jurusan
        model.addAttribute("dataAS", repository.findByOrderByCreatedAtAsc(JurusanEnum.ASTRONOMI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMA", repository.findByOrderByCreatedAtAsc(JurusanEnum.MATEMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFI", repository.findByOrderByCreatedAtAsc(JurusanEnum.FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataKI", repository.findByOrderByCreatedAtAsc(JurusanEnum.KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataBI", repository.findByOrderByCreatedAtAsc(JurusanEnum.BIOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFA", repository.findByOrderByCreatedAtAsc(JurusanEnum.FARMASI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGM", repository.findByOrderByCreatedAtAsc(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTA", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTM", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERMINYAKAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMS", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_MESIN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTK", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataEL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ELEKTRO.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataIF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INFORMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INDUSTRI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataSI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_SIPIL.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGD", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEODESI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataAR", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ARSITEKTUR.getValue(), pageable).getTotalElements());
        model.addAttribute("dataPL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PLANOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_LINGKUNGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFSRD", repository.findByOrderByCreatedAtAsc(JurusanEnum.FSRD.getValue(), pageable).getTotalElements());

        return "index";
    }

    @GetMapping("/page-trash")
    public String listDataTrash(Model model,  @RequestParam(defaultValue = "0") int page,
                           @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                           @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir, @RequestParam(name = "jurusan", required = false) String jurusan){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<TrashEntity> dataTrash = trashRepository.findAll(pageable);
        long currentPage = dataTrash.getNumber()+1;
        model.addAttribute("dataTrash", dataTrash);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);

        return "page-trash";
    }

    @GetMapping("/sekretariat")
    public String listDataSek(Model model,  @RequestParam(defaultValue = "0") int page,
                           @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                           @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir, @RequestParam(name = "jurusan", required = false) String jurusan){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        model.addAttribute("totDat", repository.count());

        //Data per jurusan
        model.addAttribute("dataAS", repository.findByOrderByCreatedAtAsc(JurusanEnum.ASTRONOMI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMA", repository.findByOrderByCreatedAtAsc(JurusanEnum.MATEMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFI", repository.findByOrderByCreatedAtAsc(JurusanEnum.FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataKI", repository.findByOrderByCreatedAtAsc(JurusanEnum.KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataBI", repository.findByOrderByCreatedAtAsc(JurusanEnum.BIOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFA", repository.findByOrderByCreatedAtAsc(JurusanEnum.FARMASI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGM", repository.findByOrderByCreatedAtAsc(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTA", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTM", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERMINYAKAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMS", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_MESIN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTK", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataEL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ELEKTRO.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataIF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INFORMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INDUSTRI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataSI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_SIPIL.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGD", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEODESI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataAR", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ARSITEKTUR.getValue(), pageable).getTotalElements());
        model.addAttribute("dataPL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PLANOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_LINGKUNGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFSRD", repository.findByOrderByCreatedAtAsc(JurusanEnum.FSRD.getValue(), pageable).getTotalElements());

        return "index";
    }

    @GetMapping("/adminAS")
    public String listDataAdminAS(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.ASTRONOMI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminMA")
    public String listDataAdminMA(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.MATEMATIKA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminFI")
    public String listDataAdminFI(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.FISIKA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminKI")
    public String listDataAdminKI(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.KIMIA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminBI")
    public String listDataAdminBI(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.BIOLOGI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminFA")
    public String listDataAdminFA(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.FARMASI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminGM")
    public String listDataAdminGM(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminGL")
    public String listDataAdminGL(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEOLOGI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTA")
    public String listDataAdminTA(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTM")
    public String listDataAdminTM(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERMINYAKAN.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminMS")
    public String listDataAdminMS(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_MESIN.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTK")
    public String listDataAdminTK(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_KIMIA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminEL")
    public String listDataAdminEL(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ELEKTRO.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTF")
    public String listDataAdminTF(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_FISIKA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminIF")
    public String listDataAdminIF(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size,
                                  @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){

        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INFORMATIKA.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTI")
    public String listDataAdminTI(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INDUSTRI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminSI")
    public String listDataAdminSI(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_SIPIL.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminGD")
    public String listDataAdminGD(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEODESI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminAR")
    public String listDataAdminAR(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ARSITEKTUR.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminPL")
    public String listDataAdminPL(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PLANOLOGI.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminTL")
    public String listDataAdminTL(Model model, @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                  @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_LINGKUNGAN.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/adminFSRD")
    public String listDataAdminFSRD(Model model, @RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                    @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data = repository.findByOrderByCreatedAtAsc(JurusanEnum.FSRD.getValue(), pageable);
        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        return "index-admin";
    }

    @GetMapping("/findByJurusan")
    public String findByJurusan(Model model, @RequestParam(defaultValue = "0") int page,
                                @RequestParam(defaultValue = "10") int size, @RequestParam(name = "sortField", defaultValue = "createdAt") String sortField,
                                @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir,
                                @RequestParam(name = "jurusan", required = false) String jurusan){
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortDir), sortField));
        Page<DataRegistrasiEntity> data;
        if(jurusan != null && jurusan != ""){
            data = repository.findByJurusan(jurusan, pageable);
        }else{
            data = repository.findAll(pageable);
        }

        long currentPage = data.getNumber()+1;
        model.addAttribute("data", data);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSize", size);
        model.addAttribute("selectedJurusan", jurusan);
        model.addAttribute("totalPages", data.getTotalPages());
        model.addAttribute("totDat", repository.count());

        //Data per jurusan
        model.addAttribute("dataAS", repository.findByOrderByCreatedAtAsc(JurusanEnum.ASTRONOMI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMA", repository.findByOrderByCreatedAtAsc(JurusanEnum.MATEMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFI", repository.findByOrderByCreatedAtAsc(JurusanEnum.FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataKI", repository.findByOrderByCreatedAtAsc(JurusanEnum.KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataBI", repository.findByOrderByCreatedAtAsc(JurusanEnum.BIOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFA", repository.findByOrderByCreatedAtAsc(JurusanEnum.FARMASI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGM", repository.findByOrderByCreatedAtAsc(JurusanEnum.GEOFISIKA_METEOROLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTA", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERTAMBANGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTM", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PERMINYAKAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataMS", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_MESIN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTK", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_KIMIA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataEL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ELEKTRO.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_FISIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataIF", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INFORMATIKA.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_INDUSTRI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataSI", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_SIPIL.getValue(), pageable).getTotalElements());
        model.addAttribute("dataGD", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_GEODESI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataAR", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_ARSITEKTUR.getValue(), pageable).getTotalElements());
        model.addAttribute("dataPL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_PLANOLOGI.getValue(), pageable).getTotalElements());
        model.addAttribute("dataTL", repository.findByOrderByCreatedAtAsc(JurusanEnum.TEKNIK_LINGKUNGAN.getValue(), pageable).getTotalElements());
        model.addAttribute("dataFSRD", repository.findByOrderByCreatedAtAsc(JurusanEnum.FSRD.getValue(), pageable).getTotalElements());
        return "index";
    }
}
