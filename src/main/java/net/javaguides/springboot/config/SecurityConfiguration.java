package net.javaguides.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import net.javaguides.springboot.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
		auth.setUserDetailsService(userService);
		auth.setPasswordEncoder(passwordEncoder());
		return auth;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/adminAS").hasRole("ADMINAS")
				.antMatchers("/adminMA").hasRole("ADMINMA")
				.antMatchers("/adminFI").hasRole("ADMINFI")
				.antMatchers("/adminKI").hasRole("ADMINKI")
				.antMatchers("/adminBI").hasRole("ADMINBI")
				.antMatchers("/adminFA").hasRole("ADMINFA")
				.antMatchers("/adminGM").hasRole("ADMINGM")
				.antMatchers("/adminGL").hasRole("ADMINGL")
				.antMatchers("/adminTA").hasRole("ADMINTA")
				.antMatchers("/adminTM").hasRole("ADMINTM")
				.antMatchers("/adminMS").hasRole("ADMINMS")
				.antMatchers("/adminTK").hasRole("ADMINTK")
				.antMatchers("/adminEL").hasRole("ADMINEL")
				.antMatchers("/adminTF").hasRole("ADMINTF")
				.antMatchers("/adminIF").hasRole("ADMINIF")
				.antMatchers("/adminTI").hasRole("ADMINTI")
				.antMatchers("/adminSI").hasRole("ADMINSI")
				.antMatchers("/adminGD").hasRole("ADMINGD")
				.antMatchers("/adminAR").hasRole("ADMINAR")
				.antMatchers("/adminPL").hasRole("ADMINPL")
				.antMatchers("/adminTL").hasRole("ADMINTL")
				.antMatchers("/adminFSRD").hasRole("ADMINFSRD")
				.antMatchers("/super-admin",
						"/edit-form",
						"/update/{id}",
						"/delete/{id}",
						"/page-trash").hasRole("ADMIN")
				.antMatchers("/sekretariat").hasRole("SEK")
				.antMatchers("/print-total-data").hasAnyRole("ADMIN", "SEK")
//				.antMatchers("/print-data").hasRole("ADMIN")
				.antMatchers("/findByJurusan").hasRole("ADMIN")
				.antMatchers("/print-data").hasAnyRole("ADMIN", "ADMINAS", "ADMINMA", "ADMINFI", "ADMINKI", "ADMINBI", "ADMINFA", "ADMINGM",
						"ADMINGL", "ADMINTA", "ADMINTM", "ADMINMS", "ADMINTK", "ADMINEL", "ADMINTF", "ADMINIF", "ADMINTI", "ADMINSI", "ADMINGD", "ADMINAR",
						"ADMINPL", "ADMINTL", "ADMINFSRD")
//				.antMatchers("/form-registrasi**").hasAnyRole("ADMIN", "ADMINAS", "ADMINMA", "ADMINFI", "ADMINKI", "ADMINBI", "ADMINFA", "ADMINGM",
//						"ADMINGL", "ADMINTA", "ADMINTM", "ADMINMS", "ADMINTK", "ADMINEL", "ADMINTF", "ADMINIF", "ADMINTI", "ADMINSI", "ADMINGD", "ADMINAR",
//						"ADMINPL", "ADMINTL", "ADMINFSRD")
				.antMatchers(
						"/registration**",
						"/form-registrasi**",
						"/save",
						"/js/**",
						"/images/**",
						"/css/**",
						"/homepage",
						"/img/**").permitAll()
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage("/login")
				.successHandler((request, response, authentication)->{
					if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))){
						response.sendRedirect("/super-admin");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINAS"))){
						response.sendRedirect("/adminAS");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINMA"))){
						response.sendRedirect("/adminMA");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINFI"))){
						response.sendRedirect("/adminFI");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINKI"))){
						response.sendRedirect("/adminKI");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINBI"))){
						response.sendRedirect("/adminBI");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINFA"))){
						response.sendRedirect("/adminFA");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINGM"))){
						response.sendRedirect("/adminGM");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINGL"))){
						response.sendRedirect("/adminGL");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTA"))){
						response.sendRedirect("/adminTA");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTM"))){
						response.sendRedirect("/adminTM");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINMS"))){
						response.sendRedirect("/adminMS");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTK"))){
						response.sendRedirect("/adminTK");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINEL"))){
						response.sendRedirect("/adminEL");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTF"))){
						response.sendRedirect("/adminTF");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINIF"))){
						response.sendRedirect("/adminIF");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTI"))){
						response.sendRedirect("/adminTI");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINSI"))){
						response.sendRedirect("/adminSI");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINGD"))){
						response.sendRedirect("/adminGD");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINAR"))){
						response.sendRedirect("/adminAR");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINPL"))){
						response.sendRedirect("/adminPL");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINTL"))){
						response.sendRedirect("/adminTL");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMINFSRD"))){
						response.sendRedirect("/adminFSRD");
					}else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_SEK"))){
						response.sendRedirect("/sekretariat");
					}
				})
				.permitAll()
				.and()
				.logout()
				.invalidateHttpSession(true)
				.clearAuthentication(true)
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/login?logout")
				.permitAll();
	}

}
