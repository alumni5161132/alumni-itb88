package net.javaguides.springboot.service;


import net.javaguides.springboot.model.DataRegistrasiEntity;
import net.javaguides.springboot.model.TrashEntity;
import net.javaguides.springboot.web.dto.DataRegistrasiDTO;

public interface DataService {
    void saveData(DataRegistrasiDTO dto);
    void updateData(Long id, DataRegistrasiDTO dto);
    DataRegistrasiEntity findById(Long id);
    void deleteData(Long id);
    void savefromtrash(String id);
}
