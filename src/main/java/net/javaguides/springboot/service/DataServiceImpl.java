package net.javaguides.springboot.service;

import lombok.var;
import net.javaguides.springboot.model.TrashEntity;
import net.javaguides.springboot.repository.TrashRepository;
import net.javaguides.springboot.web.dto.DataRegistrasiDTO;
import net.javaguides.springboot.model.DataRegistrasiEntity;
import net.javaguides.springboot.repository.DataRegistrasiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class DataServiceImpl implements DataService {

    @Autowired
    DataRegistrasiRepository repository;

    @Autowired
    TrashRepository trashRepository;

    @Override
    public void saveData(DataRegistrasiDTO dto) {
        Long id = System.currentTimeMillis();
        DataRegistrasiEntity data = new DataRegistrasiEntity();
        data.setId(id);
        data.setNama(dto.getNama());
        data.setNoHp(dto.getNoHp());
        data.setAlamat(dto.getAlamat());
        data.setJurusan(dto.getJurusan());
        data.setUkuranKemeja(dto.getUkuranKemeja());
        data.setCreatedAt(System.currentTimeMillis());

        repository.save(data);
    }

    @Override
    public void updateData(Long id, DataRegistrasiDTO dto) {
        DataRegistrasiEntity data = repository.findById(id).orElse(null);

        if(data != null){
            data.setNama(dto.getNama());
            data.setAlamat(dto.getAlamat());
            data.setNoHp(dto.getNoHp());
            data.setJurusan(dto.getJurusan());
            data.setUkuranKemeja(dto.getUkuranKemeja());
            repository.save(data);
        }
    }

    @Override
    public DataRegistrasiEntity findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void deleteData(Long id) {
        Optional<DataRegistrasiEntity> data = repository.findById(id);

        if(data.isPresent()){
            DataRegistrasiEntity dataDelete = data.get();
            TrashEntity dataTrash = new TrashEntity();
            dataTrash.setId("TR" + System.currentTimeMillis());
            dataTrash.setIdData(id);
            dataTrash.setNama(data.get().getNama());
            dataTrash.setJurusan(data.get().getJurusan());
            dataTrash.setAlamat(data.get().getAlamat());
            dataTrash.setNoHp(data.get().getNoHp());
            dataTrash.setUkuranKemeja(data.get().getUkuranKemeja());
            dataTrash.setCreatedAt(System.currentTimeMillis());
            trashRepository.save(dataTrash);
            repository.delete(dataDelete);
        }
    }

    @Override
    public void savefromtrash(String id) {
        Optional<TrashEntity> dataTrash = trashRepository.findById(id);
        DataRegistrasiEntity data = new DataRegistrasiEntity();
        TrashEntity dataDelete = dataTrash.get();
        data.setId(dataTrash.get().getIdData());
        data.setNama(dataTrash.get().getNama());
        data.setNoHp(dataTrash.get().getNoHp());
        data.setAlamat(dataTrash.get().getAlamat());
        data.setJurusan(dataTrash.get().getJurusan());
        data.setUkuranKemeja(dataTrash.get().getUkuranKemeja());
        data.setCreatedAt(System.currentTimeMillis());
        repository.save(data);
        trashRepository.delete(dataDelete);
    }
}
